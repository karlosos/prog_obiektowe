void test(char &a) {
	a = 'b';
}

//void test(char a) {
//	a = 'b';
//}


int main()
{
	char a = 'a';
	int len_a = sizeof(a);
	char *adr_a = &a;

	char &ref_a = a;
	char *adr_ref_a = &ref_a;
	int len_ref = sizeof(ref_a);
	ref_a = 'c';
	test(a);

	return 0;
}