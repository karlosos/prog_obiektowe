#include "B.h"

B::B()
{
	std::cout << "Konstruktor B" << std::endl;
}

B::B(const B& b)
{
	std::cout << "Konstruktor kopiujacy B" << std::endl;
}

B::~B()
{
	std::cout << "Destruktor B" << std::endl;
}

void B::odruch()
{
	std::cout << "Skacze" << std::endl;
}
