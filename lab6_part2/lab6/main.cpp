#include <iostream>
#include "B.h"
#include "A.h"
int main()
{
	{
		// napisac jaka jest kolejnosc wywolania konstruktorow
		// konstruktor A
		// konstruktor C // zgodnie z kolejnoscia na liscie dziedziczenia
		// konstruktor B
		B objb;

		//objb.baza(3); // wywolanie metody bazowej (z klasy bazowej)
		//trzeba inaczej
		objb.A::baza(3);
		objb.C::baza(3);


		objb.odruch(); // wywolanie metody przeslonietej
		objb.A::odruch();	// wywolanie metody z klasy bazowej

		{
			// jaka jest kolejnosc wywolywania konstruktorow kopiujacych
			// konstruktor A
			// konstruktor C
			// konstruktor kopiujacy B
			B kopia = objb;
		}


		// kolejnosc wywolywania destruktorow
		// destruktor B
		// destruktor C
		// destruktor A 
	}

	std::system("pause");
	return 0;
}