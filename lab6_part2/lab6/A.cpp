#include "A.h"

A::A()
{
	std::cout << "Konstruktor A" << std::endl;
}

A::A(const A& a)
{
	std::cout << "Konstruktor kopiujacy A" << std::endl;
}

A::~A()
{
	std::cout << "Destruktor A" << std::endl;
}

int A::baza(int a)
{
	std::cout << "Metoda klasy A" << std::endl;
	return a;
}

void A::odruch()
{
	std::cout << "Strach" << std::endl;
}