#pragma once
#include "A.h";
#include "C.h";
#include <iostream>
class B : public A, public C
{
public:
	B();
	B(const B&);
	~B();
	void odruch(); //przesloniecie funkcji
};