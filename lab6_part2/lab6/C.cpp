#include "C.h"



C::C()
{
	std::cout << "Konstruktor C" << std::endl;
}

C::C(const C& c)
{
	std::cout << "Konstruktor kopiujacy C" << std::endl;
}

C::~C()
{
	std::cout << "Destruktor C" << std::endl;
}

int C::baza(int k)
{
	std::cout << "Metoda bazowa z C" << std::endl;
	return k;
}