#include <iostream>
#include "getMax.h"
#include "Vector.h"

int main() {
	/*
	int a = -1, b = 2, c = 0;
	c = getMax<int>(a, b);
	std::cout << c << std::endl;
	c = getMax<unsigned int>(a, b);
	std::cout << c << std::endl;
	*/

	/*
	Vector<int> v(5);
	v[0] = 1;
	v[1] = 2;
	v[2] = 4;
	v[3] = 4;
	v[4] = 9;

	for (int i = 0; i < 5; i++) {
		std::cout << v[i] << std::endl;
	}
	*/

	Vector<int> obj1(2);
	obj1[0] = 1;
	obj1[1] = 1;

	Vector<int> obj2(2);
	obj2 = obj1;

	//obj2[0] = 2;

	for (int i = 0; i < 2; i++) {
		std::cout << obj1[i] << obj2[i] << std::endl;
	}

	
	Vector<char> obj3(2);
	obj3 = obj2 + obj1;

	
	for (int i = 0; i < 2; i++) {
		std::cout << obj1[i] << obj2[i] << obj3[i] << std::endl;
	}

	std::system("pause");
	
	return 0;
}