#include "Vector.h"

template<class T>
Vector <T>::Vector(int size) {
	tab = new T[size];
}

template<class T>
Vector <T>::~Vector() {
	delete[] tab;
}

template<class T>
T& Vector<T>::operator[](int index) {
	return tab[index];
}

template class Vector<int>;