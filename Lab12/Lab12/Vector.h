#pragma once


template <class T>
class Vector {
	template <class U> friend class Vector;

private:
	int size; //mowi o ilosci elementow w tablicy
	T *tab;

public:
	//konstrutor
	Vector(int size) {
		tab = new T[size];
		this->size = size;
	};
	//konkstuktor kopiujacy
	Vector(Vector<T> &ref) {
		this->size = ref.size;
		this->tab = new T[ref.size];

		for (int i = 0; i < ref.size; i++) {
			this->tab[i] = ref.tab[i];
		}
	};

	//destruktor
	~Vector() {
		delete[]tab;
	};

	//operator indeksowania
	T& operator[](int idx) {
		return tab[idx];
	};

	//operator przypisania
	Vector <T> operator = (Vector<T>&ref) {

		for (int i = 0; i < ref.size; i++) {
			this->tab[i] = ref.tab[i];
		}
		return *this;
	};

	//operator przypisania, 2, �eby dzia�alo.  Musiy zaprzyjznic nowy class
	template <class U>
	Vector <T> operator = (Vector<U>&ref) {

		for (int i = 0; i < ref.size; i++) {
			this->tab[i] = ref.tab[i];
		}
		return *this;
	};

	//operator dodawania        T i U, dwa razy musi by�, nie mzoe byc przyjazni miedzy int a int
	Vector<T> operator+(Vector<T> &ref) {
		Vector<T> wynik(ref.size);

		for (int i = 0; i < ref.size; i++) {
			wynik.tab[i] = this->tab[i] + ref.tab[i];
		}

		return wynik;
	};

	//operator dodawania szablonowy
	template <class U>
	Vector<T> operator+(Vector<U> &ref) {
		Vector<T> wynik(ref.size);

		for (int i = 0; i < ref.size; i++) {
			wynik.tab[i] = this->tab[i] + ref.tab[i];
		}
		return wynik;
	};

};
