#include "getMax.h"



template<class  T>

T getMax(T a, T b)
{
	if (a > b) return a;
	return b;
}

template int getMax<int>(int, int);
template unsigned int getMax<unsigned>(unsigned int, unsigned int);