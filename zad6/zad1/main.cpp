#include "test.h"

int main()
{
	test(1); // wywoluje funkcje 1 - tylko ona pasuje do liczby parametrow
	test(1, 'a'); // wywoluje funkcje 2 bez rzutowan
	test(1.); // wywoluje funkcje 1 z rzutowaniem z double na int
	test('a'); // wywoluje funkcje 1 z rzutowaniem z char na int

	test(1., 1.); // brak jednoznacznosci, moze wywolac funkcje 2 i 3
	test('a', 'b'); // brak jednoznacznosci, moze wywolac funkcje 2 i 3
	test((int)1., 1.); // rzutujemy pierwszu argument na int dlatego jednoznacznie przypasowuje do funkcji 2
	test(1., (char)1.); // rzutujemy drugi argument na char dlatego jednoznacznie przypasowuje do funkcji 2
	test(1., (int)1.); // rzutujemy drug argum na int dlatego jednoznacznie wybiera funkcje 3
	test((char)1., 1); // rzutujemy pierwsz argum na char dlatego jednoznacznie wybiera funkcje 3

}