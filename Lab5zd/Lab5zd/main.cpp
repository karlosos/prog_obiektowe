#include "Int.h"
#include <iostream>

void printInt(Int i) {
	std::cout << i.getValue() << std::endl;
}

int main() {
	Int my_int1(60);
	Int my_int2(20);

	// dodawanie
	if ((my_int1 + my_int2).getValue() != 80)
		std::cout << "Dodawanie" << std::endl;
	if ((60 + my_int2).getValue() != 80)
		std::cout << "Dodawanie" << std::endl;
	if ((my_int1 + 20).getValue() != 80)
		std::cout << "Dodawanie" << std::endl;
	// odejmowanie
	if ((my_int1 - my_int2).getValue() != 40)
		std::cout << "Odejmowanie" << std::endl;
	if ((60 - my_int2).getValue() != 40)
		std::cout << (60 - my_int2).getValue() << std::endl;
	if ((my_int1 - 20).getValue() != 40)
		std::cout << "Odejmowanie" << std::endl;
	// mnozenie
	if ((my_int1 * my_int2).getValue() != 1200)
		std::cout << (my_int1 * my_int2).getValue() << std::endl;
	if ((60 * my_int2).getValue() != 1200)
		std::cout << (60 * my_int2).getValue() << std::endl;
	if ((my_int1 * 20).getValue() != 1200)
		std::cout << (my_int1 * 20).getValue() << std::endl;
	// dzielenie
	if ((my_int1 / my_int2).getValue() != 3)
		std::cout << (my_int1 / my_int2).getValue() << std::endl;
	if ((60 / my_int2).getValue() != 3)
		std::cout << (60 / my_int2).getValue() << std::endl;
	if ((my_int1 / 20).getValue() != 3)
		std::cout <<  (my_int1 / 20).getValue() << std::endl;

	return 0;
}