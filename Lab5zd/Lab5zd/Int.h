#pragma once
class Int
{
public:
	// Konstruktor z domniemana wartoscia
	Int(int value = 0);
	// Konstruktor kopiujacy
	Int(const Int & i);
	// Destruktor
	~Int();
	// Setter pola value
	void setValue(int value);
	// Getter pola value
	int getValue() const;

	// Przeciazenie operatora przypisania. Przypisanie innym obiektem Int
	Int& operator=(const Int & i);
	// Przeciazenie operatora przypisania. Przypisanie wartoscia integer
	Int& operator=(int value);

	// Przeciazenie operatora dodawania
	friend Int operator+(const Int &, const Int &);
	friend Int operator+(const Int &, int);
	friend Int operator+(int, const Int &);
	// Przeciazenie operatora odejmowania
	friend Int operator-(const Int &, const Int &);
	friend Int operator-(const Int &, int);
	friend Int operator-(int, const Int &);
	// Przeciazenie operatora mnozenia
	friend Int operator*(const Int &, const Int &);
	friend Int operator*(const Int &, int);
	friend Int operator*(int, const Int &);
	// Przeciazenie operatora dzielenia
	friend Int operator/(const Int &, const Int &);
	friend Int operator/(const Int &, int);
	friend Int operator/(int, const Int &);
	// Przeciazenie operatora porownania
	friend bool operator<(const Int &, const Int &);
	friend bool operator<(const Int &, int);
	friend bool operator<(int, const Int &);	
	friend bool operator<=(const Int &, const Int &);
	friend bool operator<=(const Int &, int);
	friend bool operator<=(int, const Int &);	
	friend bool operator>(const Int &, const Int &);
	friend bool operator>(const Int &, int);
	friend bool operator>(int, const Int &);	
	friend bool operator>=(const Int &, const Int &);
	friend bool operator>=(const Int &, int);
	friend bool operator>=(int, const Int &);	
	friend bool operator==(const Int &, const Int &);
	friend bool operator==(const Int &, int);
	friend bool operator==(int, const Int &);
	friend bool operator!=(const Int &, const Int &);
	friend bool operator!=(const Int &, int);
	friend bool operator!=(int, const Int &);
private:
	int *value;
	void clear();
};
