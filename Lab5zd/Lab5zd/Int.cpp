#include "Int.h"
// Konstruktor z wartoscia domyslna
Int::Int(int value)
{
	this->value = new int(value);
}
// Konstruktor kopiujacy
Int::Int(const Int & i)
{
	this->value = nullptr;
	if (i.value != nullptr)
		this->setValue(i.getValue());
}
// Destruktor
Int::~Int()
{
	clear();
}
// Czyszczenie pamieci zarezerwowanej przez obiekt na zewnatrz
void Int::clear()
{
	if (this->value != nullptr)
		delete(this->value);
}
// Setter pola value
void Int::setValue(int value)
{
	clear();
	this->value = new int(value);
}
// Getter pola value
int Int::getValue() const {
	return *(this->value);
}
// Przeciazenie operatora przypisania
Int& Int::operator=(const Int & i)
{
	this->setValue(i.getValue());
	return *this;
}
// Przeciazenie operatora przypisania z wartoscia calkowita
Int& Int::operator=(int value)
{
	this->setValue(value);
	return *this;
}

// Przeciazenie operatora dodawania
Int operator+(const Int & f, const Int & s)
{
	return Int(f.getValue() + s.getValue());
}
Int operator+(const Int & f, int value)
{
	return Int(f.getValue() + value);
}
Int operator+(int value, const Int & f)
{
	return Int(f.getValue() + value);
}

// Przeciazenie operatora odejmowania
Int operator-(const Int & f, const Int & s)
{
	return Int(f.getValue() - s.getValue());
}
Int operator-(const Int & f, int value)
{
	return Int(f.getValue() - value);
}
Int operator-(int value, const Int & f)
{
	return Int(value - f.getValue());
}

// Przeciazenie operatora mnozenia
Int operator*(const Int & f, const Int & s)
{
	return Int(f.getValue() * s.getValue());
}
Int operator*(const Int & f, int value)
{
	return Int(f.getValue() * value);
}
Int operator*(int value, const Int & f)
{
	return Int(f.getValue() * value);
}

// Przeciazenie operatora dzielenia
Int operator/(const Int & f, const Int & s)
{
	return Int(f.getValue() / s.getValue());
}
Int operator/(const Int & f, int value)
{
	return Int(f.getValue() / value);
}
Int operator/(int value, const Int & f)
{
	return Int(value / f.getValue());
}

// Przeciazenie operatorow porownania
bool operator<(const Int & left, const Int & right)
{
	return (*(left.value) < *(right.value));
}
bool operator<(const Int & left, int right)
{
	return (*(left.value) < right);
}
bool operator<(int left, const Int & right)
{
	return (left < *(right.value));
}

bool operator>(const Int & left, const Int & right){ return right < left; }
bool operator>(const Int & left, int right){ return right < left; }
bool operator>(int left, const Int & right){ return right < left; }
bool operator<=(const Int & left, const Int & right){ return !(left > right); }
bool operator<=(const Int & left, int right){ return !(left > right); }
bool operator<=(int left, const Int & right){ return !(left > right); }
bool operator>=(const Int & left, const Int & right){ return !(left < right); }
bool operator>=(const Int & left, int right){ return !(left < right); }
bool operator>=(int left, const Int & right){ return !(left < right); }

bool operator==(const Int & left, const Int & right)
{
	return (*(left.value) == *(right.value));
}
bool operator==(const Int & left, int right)
{
	return (*(left.value) == right);
}
bool operator==(int left, const Int & right)
{
	return (right == left);
}
bool operator!=(const Int & left, const Int & right)
{
	return !(left == right);
}
bool operator!=(const Int & left, int right)
{
	return !(left == right);
}
bool operator!=(int left, const Int & right)
{
	return !(left == right);
}
