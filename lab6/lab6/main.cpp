#include <iostream>
#include "B.h"
#include "A.h"
int main()
{
	{
		// napisac jaka jest kolejnosc wywolania konstruktorow
		// konstruktor A
		// konstruktor B
		B objb;

		objb.baza(3); // wywolanie metody bazowej (z klasy bazowej)

		objb.odruch(); // wywolanie metody przeslonietej
		objb.A::odruch();	// wywolanie metody z klasy bazowej

		{
			// jaka jest kolejnosc wywolywania konstruktorow kopiujacych
			// konstruktor A
			// konstruktor kopiujacy B
			B kopia = objb;
		}


		// kolejnosc wywolywania destruktorow
		// destruktor B
		// destruktor A 
	}

	std::system("pause");
	return 0;
}