#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "Osoba.h"
#include "test.h"

int main() {
	// kiedy konstruktor kopiujacy
	// przy przekazywaniu obiektu do funkcji
	// przy inicjalizacji np Osoba kopia(osoba), lub Osoba kopia = osoba;
	// w drugim przypadku to nie jest operator przypisania
	// przy zwracania wartosci obiekt

	Osoba osoba;
	osoba.setImie("Karol");
	std::cout << osoba.getImie() << std::endl;
	// konstruktor kopiujacy przy przekazywaniu obiektu do funkcji
	test(osoba);

	{
		// konstruktor kopiujacy przy inicjalizacji obiektu innym obiektem
		Osoba kopia(osoba);
		std::cout << kopia.getImie() << std::endl;
	}

	{
		// konstruktor kopiujacy przy inicjalizacji obiektu innym obiektem
		Osoba kopia = osoba;
		std::cout << kopia.getImie() << std::endl;
	}

	{
		std::cout << test2().getImie() << std::endl;
	}

	{
		Osoba osoba2;
		// przypisanie
		// trzeba przeciazyc operator przypisania
		osoba2 = osoba;
		std::cout << osoba2.getImie() << std::endl;
	}

	{
		Osoba osoba2;
		osoba2 = "Ewa";
		std::cout << osoba2.getImie() << std::endl;
	}
	
	std::system("pause");
	return 0;
}