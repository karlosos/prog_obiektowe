#pragma once
class Osoba
{
public:
	Osoba();
	~Osoba();
	Osoba(const Osoba &o);
	void setImie(char *imie);
	char* getImie();
	Osoba& operator=(const Osoba &);
	Osoba& operator=(char * imie);
private:
	char *imie;
	void clear();
};

