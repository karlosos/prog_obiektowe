#include "Osoba.h"
#include <string>


Osoba::Osoba()
{
	this->imie = nullptr;
}


Osoba::~Osoba()
{
	clear();
}

Osoba::Osoba(const Osoba& o)
{
	this->imie = nullptr;
	if (o.imie != nullptr)
		this->setImie(o.imie);
}

Osoba& Osoba::operator=(const Osoba & o)
{
	this->setImie(o.imie);
	return *this;
}

Osoba& Osoba::operator=(char * imie)
{
	this->setImie(imie);
	/*clear();
	this->imie = new char[strlen(imie) + 1];
	strcpy(this->imie, imie);*/
	return *this;
}

void Osoba::clear()
{
	if (this->imie != nullptr) {
		delete(imie);
	}
}

void Osoba::setImie(char* imie)
{
	clear();
	this->imie = new char[strlen(imie) + 1];
	strcpy(this->imie, imie);
}

char* Osoba::getImie()
{
	return this->imie;
}