#include "Matematyk.h"
#include "Polonista.h"
#include <iostream>
int main()
{
	//Polonista* objp = new Polonista();
	//Ekspert* obje = new Polonista();
	//delete obje;
	//delete(objp);
	Ekspert* obje[3];
	obje[0] = new Polonista();
	obje[1] = new Matematyk();
	obje[2] = new Ekspert();

	for (int i = 0; i < 3; i++) {
		std::cout << obje[i]->dziedzina() << std::endl;
	}

	for (int i = 0; i < 3; i++) {
		delete obje[i];
	}

	std::system("pause");
	return 0;
}