#pragma once
#include "Ekspert.h"
#include <iostream>
class Matematyk :
	public Ekspert
{
public:
	Matematyk();
	Matematyk(const Matematyk&);
	~Matematyk();
	char* dziedzina();
};

