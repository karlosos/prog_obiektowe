#include "OknoUchylnoOtwierane.h"



OknoUchylnoOtwierane::OknoUchylnoOtwierane()
{
	std::cout << "Konstruktor okna uchylno otiweranego" << std::endl;
}


OknoUchylnoOtwierane::~OknoUchylnoOtwierane()
{
	std::cout << "Destrutkro okna uchylno otwieranego" << std::endl;
}

void OknoUchylnoOtwierane::otworz()
{
	OknoOtwierane::otworz();
}

void OknoUchylnoOtwierane::obroc()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Nie moge obrocic okna" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}

void OknoUchylnoOtwierane::uchyl()
{
	OknoUchylne::uchyl();
}

void OknoUchylnoOtwierane::zamknij()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Zamykam okno" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
