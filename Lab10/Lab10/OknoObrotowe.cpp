#include "OknoObrotowe.h"



OknoObrotowe::OknoObrotowe()
{
	std::cout << "Konstruktor okna obrotowego" << std::endl;
}


OknoObrotowe::~OknoObrotowe()
{
	std::cout << "Konstruktor okna obrotowego" << std::endl;
}



void OknoObrotowe::otworz()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Nie moge otworzyc okna" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoObrotowe::obroc()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Obracam okno" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoObrotowe::uchyl()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Nie moge uchylic okna" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoObrotowe::zamknij()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Zamykam okno" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}