#include "Szklarz.h"
#include "OknoOtwierane.h"
#include "OknoObrotowe.h"
#include "OknoUchylne.h"
#include "OknoUchylnoOtwierane.h"

int main() {
	Szklarz szklarz;
	Okno* okna[4];
	okna[0] = new OknoOtwierane();
	okna[1] = new OknoUchylne();
	okna[2] = new OknoObrotowe();
	okna[3] = new OknoUchylnoOtwierane();

	for (int i = 0; i < 4; i++) {
		test(okna[i]);
	}

	for (int i = 0; i < 4; i++) {
		szklarz.oszklij(okna[i]);
	}

	for (int i = 0; i < 4; i++) {
		test(okna[i]);
	}

	for (int i = 0; i < 4; i++) {
		delete okna[i];
	}

	std::system("pause");
	return 0;
}