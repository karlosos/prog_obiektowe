#pragma once
#include "OknoUchylne.h"
#include "OknoOtwierane.h"
class OknoUchylnoOtwierane : 
	public OknoUchylne, public OknoOtwierane
{
public:
	OknoUchylnoOtwierane();
	~OknoUchylnoOtwierane();

	void otworz();
	void obroc();
	void uchyl();
	void zamknij();
};

