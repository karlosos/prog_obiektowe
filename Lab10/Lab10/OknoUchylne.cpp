#include "OknoUchylne.h"



OknoUchylne::OknoUchylne()
{
	std::cout << "Konstruktor okna uchylnego" << std::endl;
}


OknoUchylne::~OknoUchylne()
{
	std::cout << "Destruktor okna uchylnego" << std::endl;
}

void OknoUchylne::otworz()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Nie moge otworzyc okna" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoUchylne::obroc()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Nie mooge obrocic okna" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoUchylne::uchyl()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Uchylam okno" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoUchylne::zamknij()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Zamykam okno" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}