#pragma once
#include "Okno.h"
class OknoObrotowe :
	public Okno
{
public:
	OknoObrotowe();
	~OknoObrotowe();
	void otworz();
	void obroc();
	void uchyl();
	void zamknij();
};

