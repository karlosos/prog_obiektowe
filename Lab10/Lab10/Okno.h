#pragma once
#include <iostream>
class Szklarz;
class Okno
{
	friend class Szklarz;

private:
	int szyba;
public:
	Okno();
	virtual ~Okno();
	virtual void otworz() = 0;
	virtual void obroc() = 0;
	virtual void uchyl() = 0;
	virtual void zamknij() = 0;
	int jestSzyba();
};

void test(Okno* o);