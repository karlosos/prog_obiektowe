#pragma once
#include "Okno.h"
#include <iostream>
class OknoOtwierane :
	public virtual Okno
{
public:
	OknoOtwierane();
	~OknoOtwierane();
	void otworz();
	void obroc();
	void uchyl();
	void zamknij();
};

