#pragma once
#include "Okno.h"
class OknoUchylne :
	public virtual Okno
{
public:
	OknoUchylne();
	~OknoUchylne();
	void otworz();
	void obroc();
	void uchyl();
	void zamknij();
};

