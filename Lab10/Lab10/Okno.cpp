#include "Okno.h"



Okno::Okno()
{
	std::cout << "Konstruktor okna" << std::endl;
	szyba = 0;
}


Okno::~Okno()
{
	std::cout << "Destruktor okna" << std::endl;
}

int Okno::jestSzyba() 
{
	return szyba;
}


void test(Okno* o) {
	o->otworz();
	o->obroc();
	o->uchyl();
	o->zamknij();
}