#include "OknoOtwierane.h"



OknoOtwierane::OknoOtwierane()
{
	std::cout << "Konstruktor okna otwieranego" << std::endl;
}


OknoOtwierane::~OknoOtwierane()
{
	std::cout << "Destruktor okna otiweranego" << std::endl;
}

void OknoOtwierane::otworz()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Otwieram okno" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoOtwierane::obroc()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Nie moge obrocic okna otwieranego" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoOtwierane::uchyl()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Nie moge uchylic okna otwieranego" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}
void OknoOtwierane::zamknij()
{
	if (Okno::jestSzyba() == 1) {
		std::cout << "Zamykam okno otwieranego" << std::endl;
	}
	else {
		std::cout << "Okno nie ma szyby" << std::endl;
	}
}