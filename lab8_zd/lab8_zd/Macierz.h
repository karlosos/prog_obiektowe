#pragma once
#include <vector>
#include <iostream>
#include "Wiersz.h"

class Macierz
{
public:
	Macierz(unsigned int wiersze = 0, unsigned int kolumny = 0);	// konstruktor
	Macierz(const Macierz& o);										// konstruktor kopiujacy
	~Macierz();														// destruktor
	int& operator()(int wiersz, int kolumna);		// operator indeksowania (wiersz, kolumna)
	Wiersz& operator[](int wiersz);					// operator indeksowanie [wiersz][kolumna]
	void wypisz();									// wypisuje macierz na ekran

	// operator przypisania
	Macierz& operator=(const Macierz& b);

	// operatory dodawania
	Macierz operator+(Macierz& b);
	Macierz operator+(int b);
	friend Macierz operator+(int b, Macierz& a);

	// operatory dodawania
	Macierz operator-(Macierz& b);
	Macierz operator-(int b);
	friend Macierz operator-(int b, Macierz& a);

	// operator porownania
	bool operator==(Macierz& b);
	bool operator!=(Macierz& b);

	// operator mnozenia
	Macierz operator*(Macierz& b);
	Macierz operator*(int b);
	friend Macierz operator*(int b, Macierz& a);

private:
	int liczba_wierszy;
	int liczba_kolumn;
	std::vector<Wiersz> wiersze;
};

