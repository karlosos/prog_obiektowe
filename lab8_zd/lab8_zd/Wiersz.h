#pragma once
#include <vector>
class Wiersz
{
public:
	Wiersz(int kolumny);
	Wiersz(const Wiersz& o);
	~Wiersz();
	int& operator[](int kolumna);
private:
	std::vector<int> elementy;
	friend class Macierz;
};

