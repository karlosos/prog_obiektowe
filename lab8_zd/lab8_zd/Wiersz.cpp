#include "Wiersz.h"

// Konstruktor wiersza
Wiersz::Wiersz(int kolumny)
{
	for (int i = 0; i < kolumny; i++) {
		elementy.push_back(0);
	}
}

// Konstruktor kopiujacy wiersza
Wiersz::Wiersz(const Wiersz& o)
{
	elementy.empty();
	for (int i = 0; i < o.elementy.size(); i++) {
		elementy.push_back(o.elementy.at(i));
	}
}

// Destruktor wiersza
Wiersz::~Wiersz()
{
	elementy.empty();
}

// Operator indeksowania wiersza
int& Wiersz::operator[](int kolumna)
{
	return elementy.at(kolumna - 1);
}