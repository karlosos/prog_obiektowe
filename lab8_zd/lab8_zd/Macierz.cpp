#include "Macierz.h"


// Konstruktor
Macierz::Macierz(unsigned int l_wierszy, unsigned int l_kolumn) : liczba_wierszy(l_wierszy), liczba_kolumn(l_kolumn)
{
	for (int i = 0; i < l_wierszy; i++) {
		Wiersz wiersz = Wiersz(l_kolumn);
		wiersze.push_back(wiersz);
	}
}

// Konstruktor kopiujacy
Macierz::Macierz(const Macierz& o) : liczba_wierszy(o.liczba_wierszy), liczba_kolumn(o.liczba_kolumn)
{
	wiersze.clear();
	for (int i = 0; i < o.liczba_wierszy; i++) {
		wiersze.push_back(o.wiersze.at(i));
	}
}

// Destruktor
Macierz::~Macierz()
{
	wiersze.clear();
}

// Operator indeksowania przez ()
int& Macierz::operator()(int wiersz, int kolumna)
{
	return wiersze.at(wiersz - 1)[kolumna];
}

// Operator ideksowania przez [][]
Wiersz& Macierz::operator[](int wiersz)
{
	return wiersze.at(wiersz - 1);
}

// Wypisywanie macierzy na ekran
void Macierz::wypisz()
{
	for (int i = 0; i < liczba_wierszy; i++) {
		for (int j = 0; j < liczba_kolumn; j++) {
			std::cout << wiersze.at(i).elementy.at(j) << " ";
		}
		std::cout << std::endl;
	}
}

// operator przypisania
Macierz& Macierz::operator=(const Macierz& b)
{
	this->liczba_kolumn = b.liczba_kolumn;
	this->liczba_wierszy = b.liczba_wierszy;
	wiersze.clear();
	for (int i = 0; i < b.liczba_wierszy; i++) {
		wiersze.push_back(b.wiersze.at(i));
	}
	return *this;
}

// operator dodawania
Macierz Macierz::operator+(Macierz& b)
{
	if (this->liczba_wierszy != b.liczba_wierszy || this->liczba_kolumn != b.liczba_kolumn)
		return false;
	Macierz tmp = *this;
	for (int i = 1; i <= liczba_wierszy; i++) {
		for (int j = 1; j <= liczba_kolumn; j++) {
			tmp.operator[](i)[j] += b.operator()(i,j);
		}
	}
	return tmp;
}

// operator dodawania
Macierz Macierz::operator+(int b)
{
	Macierz tmp = *this;
	for (int i = 1; i <= liczba_wierszy; i++) {
		for (int j = 1; j <= liczba_kolumn; j++) {
			tmp.operator[](i)[j] += b;
		}
	}
	return tmp;
}

// operator dodawania
Macierz operator+(int b, Macierz& a)
{
	Macierz tmp = a;
	for (int i = 1; i <= a.liczba_wierszy; i++) {
		for (int j = 1; j <= a.liczba_kolumn; j++) {
			tmp.operator[](i)[j] += b;
		}
	}
	return tmp;
}

// operator odejmowania
Macierz Macierz::operator-(Macierz& b)
{
	if (this->liczba_wierszy != b.liczba_wierszy || this->liczba_kolumn != b.liczba_kolumn)
		return false;
	Macierz tmp = *this;
	for (int i = 1; i <= liczba_wierszy; i++) {
		for (int j = 1; j <= liczba_kolumn; j++) {
			tmp.operator[](i)[j] -= b.operator()(i, j);
		}
	}
	return tmp;
}

// operator odejmowania
Macierz Macierz::operator-(int b)
{
	return operator+(b * (-1));
}

// operator odejmowania
Macierz operator-(int b, Macierz& a)
{
	return operator+(b * (-1), a);
}

// operator porownania
bool Macierz::operator==(Macierz& b)
{
	if (this->liczba_kolumn != b.liczba_kolumn || this->liczba_wierszy != b.liczba_wierszy)
		return false;
	for (int i = 1; i <= liczba_wierszy; i++) {
		for (int j = 1; j <= liczba_kolumn; j++) {
			if (this->operator[](i)[j] != b[i][j])
				return false;
		}
	}
	return true;
}

// operator porownania
bool Macierz::operator!=(Macierz& b)
{
	if (this->operator==(b))
		return false;
	else
		return true;
}

// operator mnozenia
Macierz Macierz::operator*(Macierz& b)
{
	if (this->liczba_kolumn != b.liczba_wierszy)
		return false;
	Macierz tmp = Macierz(this->liczba_wierszy, b.liczba_kolumn);

	for (int i = 1; i <= liczba_wierszy; i++) {
		for (int j = 1; j <= b.liczba_kolumn; j++) {
			int sum = 0;
			for (int k = 1; k <= liczba_kolumn; k++) {
				sum += this->operator[](i)[k] * b[k][j];
			}
			tmp[i][j] = sum;
		}
	}
	return tmp;
}

// operator mnozenia
Macierz Macierz::operator*(int b)
{
	Macierz tmp = *this;
	for (int i = 1; i <= liczba_wierszy; i++) {
		for (int j = 1; j <= liczba_kolumn; j++) {
			tmp.operator[](i)[j] *= b;
		}
	}
	return tmp;
}

// operator mnozenia
Macierz operator*(int b, Macierz& a)
{
	Macierz tmp = a;
	for (int i = 1; i <= a.liczba_wierszy; i++) {
		for (int j = 1; j <= a.liczba_kolumn; j++) {
			tmp.operator[](i)[j] *= b;
		}
	}
	return tmp;
}