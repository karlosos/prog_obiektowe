#include "Macierz.h"

int main() {
	Macierz a(3, 5);
	{
		Macierz b(8, 5);
		b = b + 99;
		a = b;
	}

	
	a.wypisz();
	std::cout << std::endl;
	a(1,1) = 8;
	a.wypisz();
	std::cout << std::endl;
	(a + 9).wypisz();
	std::cout << std::endl;
	a = a + 2;
	a.wypisz();
	std::cout << std::endl;
	(a - 3).wypisz();
	std::cout << std::endl;
	a = a - 5;
	a.wypisz();

	Macierz b = a;
	Macierz c;
	c = a;
	if (a == b) {
		std::cout << "a i b rowne" << std::endl;
	}
	if (a == c) {
		std::cout << "a i c rowne" << std::endl;
	}
	c[2][3] = 9;
	if (a != c) {
		std::cout << "a i c nierowne" << std::endl;
	}

	a = Macierz(3, 4);
	a[1][1] = 1;
	a[1][2] = 2;
	a[1][3] = 3;
	a[1][4] = 4;

	a[2][1] = 5;
	a[2][2] = 6;
	a[2][3] = 7;
	a[2][4] = 8;

	a[3][1] = 9;
	a[3][2] = 8;
	a[3][3] = 7;
	a[3][4] = 6;
	a.wypisz();
	std::cout << std::endl;
	b = Macierz(4, 5);
	b[1][1] = 4;
	b[1][2] = 3;
	b[1][3] = 2;
	b[1][4] = 1;
	b[1][5] = 2;

	b[2][1] = 3;
	b[2][2] = 4;
	b[2][3] = 5;
	b[2][4] = 6;
	b[2][5] = 7;

	b[3][1] = 8;
	b[3][2] = 9;
	b[3][3] = 8;
	b[3][4] = 7;
	b[3][5] = 6;

	b[4][1] = 5;
	b[4][2] = 4;
	b[4][3] = 3;
	b[4][4] = 2;
	b[4][5] = 1;
	b.wypisz();
	std::cout << std::endl;
	c = a*b;
	c.wypisz();
	std::system("pause");
	return 0;
}