#pragma once
class List;

class Node
{
	friend class List;

public:
	List* getList();
	void* getValue();
	void setValue(void *);
private:
	Node();
	Node(List* list);
	Node(void* value, List* list);
	~Node();
	Node *next;
	Node *prev;
	List *list;
	void *value;
};

