#pragma once
#include "Node.h";
#include <iostream>
class List
{
public:
	List();
	~List();
	bool remove(Node* node);
	Node* insert();
	void print();

private:
	Node* head;
	Node* tail;

};

