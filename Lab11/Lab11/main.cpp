#include "List.h"
#include "Node.h"
#include <iostream>

int main() {
	//Node node;
	//Node* node2 = new Node();

	List list;
	Node *node1 = list.insert();
	node1->setValue(new int(321321));
	Node *node2 = list.insert();
	node2->setValue(new int(1879));
	list.print();
	list.remove(node2);
	list.print();
	Node *node3 = list.insert();
	node3->setValue(new int(2137));
	list.print();

	std::system("pause");
	return 0;
}