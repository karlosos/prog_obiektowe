#include "List.h"



List::List()
{
	head = nullptr;
	tail = nullptr;
}


List::~List()
{
}

Node* List::insert()
{
	Node* node = new Node(this);
	// wstawiamy pierwszy
	if (head == nullptr) {
		head = node;
		tail = node;
		return node;
	}
	else {
		tail->next = node;
		node->prev = tail;
		tail = node;
		return node;
	}
}

bool List::remove(Node* node)
{
	if (node->list == this) {
		Node* prev = node->prev;
		Node* next = node->next;
		if (head == node) {
			head = next;
		}
		else {
			prev->next = next;
		}

		if (tail == node) {
			tail = prev;
		} else {
			next->prev = prev;
		}

		delete node;
		return true;
	}
	return false;
}

void List::print()
{
	std::cout << "Print" << std::endl;
	Node* cur = head;
	while (cur) {
		std::cout << "val:" << *( int* ) cur->getValue() << std::endl;
		cur = cur->next;
	}
}