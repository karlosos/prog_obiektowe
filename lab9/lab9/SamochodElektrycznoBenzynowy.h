#pragma once
#include "SamochodElektryczny.h"
#include "SamochodBenzynowy.h"
class SamochodElektrycznoBenzynowy :
	public SamochodElektryczny, public SamochodBenzynowy
{
public:
	SamochodElektrycznoBenzynowy();
	SamochodElektrycznoBenzynowy(const SamochodElektrycznoBenzynowy&);
	virtual ~SamochodElektrycznoBenzynowy();
	int uruchom();
	int tankuj();
	int jedz();
	int stop();
	int testSilnika();
};

