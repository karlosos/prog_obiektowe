#include "SamochodBenzynowoGazowy.h"
#include "SamochodBenzynowy.h"
#include "SamochodGazowy.h"

SamochodBenzynowoGazowy::SamochodBenzynowoGazowy()
{
	std::cout << "Konstruktor samochodu bg" << std::endl;
}

SamochodBenzynowoGazowy::SamochodBenzynowoGazowy(const SamochodBenzynowoGazowy&)
{
	std::cout << "Konstruktor kopiujacy samochodu bg" << std::endl;
}

SamochodBenzynowoGazowy::~SamochodBenzynowoGazowy()
{
	std::cout << "Destruktor samochodu bg" << std::endl;
}

int SamochodBenzynowoGazowy::uruchom()
{
	if (silnik) {
		std::cout << "Nie moge uruchomic silnika spalinowego bo jest juz uruchomiony" << std::endl;
		return -1;
	}
	else {
		silnik = 1;
		std::cout << "Uruchamiam silnik spalinowy" << std::endl;
		return 1;
	}
}

int SamochodBenzynowoGazowy::tankuj()
{
	SamochodGazowy::tankuj();
	SamochodBenzynowy::tankuj();
	return 0;
}

int SamochodBenzynowoGazowy::jedz()
{
	if (butla) {
		SamochodGazowy::jedz();
		return 1;
	}
	else if (bak) {
		SamochodBenzynowy::jedz();
		return 1;
	}
	else {
		std::cout << "Nie moge jechac bo butla i bak sa puste" << std::endl;
		return 0;
	}

}

int SamochodBenzynowoGazowy::stop()
{
	SamochodGazowy::stop();
	SamochodBenzynowy::stop();
	return 0;
}

int SamochodBenzynowoGazowy::testSilnika()
{
	int ret = 1;
	std::cout << "test benzyny";
	ret = SamochodBenzynowy::testSilnika() && ret;
	std::cout << "test elektryczny";
	ret = SamochodGazowy::testSilnika() && ret;
	return ret;
}