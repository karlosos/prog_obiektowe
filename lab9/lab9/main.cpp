#include "Samochod.h"
#include "SamochodBenzynowy.h"
#include "SamochodElektryczny.h"
#include "SamochodBenzynowoElektryczny.h"
#include "SamochodElektrycznoBenzynowy.h"
#include "SamochodBenzynowoGazowy.h"

void testDrive(Samochod* s) {
	s->uruchom();
	s->tankuj();
	s->jedz();
	s->jedz();
	s->stop();
}

int main()
{
	//
	// samochod benzynowo elektryczny
	//
	/*
	{
		SamochodHybrydowy sh;
		uszkodzenie((SamochodElektryczny*)&sh);
		sh.testSilnika();
		naprawa((SamochodBenzynowy*)&sh);
		sh.testSilnika();
		naprawa((SamochodElektryczny*)&sh);
		sh.testSilnika();
	}
	*/
	{
		SamochodBenzynowoElektryczny sh;
		uszkodzenie((SamochodBenzynowy*)&sh);
		sh.testSilnika();
		naprawa((SamochodBenzynowy*)&sh);
		sh.testSilnika();
	}
	
	{
		SamochodElektrycznoBenzynowy sh;
		uszkodzenie((SamochodBenzynowy*)&sh);
		sh.testSilnika();
		naprawa((SamochodBenzynowy*)&sh);
		sh.testSilnika();
	}

	{
		SamochodBenzynowoGazowy sbg;
		uszkodzenie((SamochodBenzynowy*)&sbg);
		sbg.testSilnika();
		naprawa((SamochodBenzynowy*)&sbg);
		sbg.testSilnika();
		sbg.tankuj();
		sbg.uruchom();
		sbg.jedz();
		sbg.jedz();
		sbg.jedz();
	}

	/*
	przy wirtualnym sb i sg
	SBG(S v | SB | SG | SBG)

	// zalezy od kolennosci dziedziczenia.
	// dziedziczone w sposob wirtualny jest wywolywany jako pierwszy
	// kolejnosc wywolywania konstruktorow w zaleznosci od dziedziczenia
	SEB(S v | S | SE | SB | SEB)
	
	SBE(S v | SB | S | SE | SBE )
	*/

	/*
	SamochodElektryczny se;
	SamochodBenzynowy sb;
	SamochodHybrydowy sh;
	std::cout << std::endl;
	std::cout << "Jazda elektrycznym" << std::endl;
	testDrive(&se);
	std::cout << std::endl;
	std::cout << "Jazda benzynowym" << std::endl;
	testDrive(&sb);
	std::cout << std::endl;
	std::cout << "Jazda hybrydowym" << std::endl;
	testDrive((SamochodBenzynowy*)&sh);
	std::system("pause");
	*/
	std::system("pause");
	return 0;
}