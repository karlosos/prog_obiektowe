#include "SamochodElektryczny.h"



SamochodElektryczny::SamochodElektryczny() : akumulator(0)
{
	std::cout << "Konstruktor samochodu elektrycznego" << std::endl;
}

SamochodElektryczny::SamochodElektryczny(const SamochodElektryczny&)
{
	std::cout << "Konstruktor kopiujacy samochodu elektrycznego" << std::endl;
}

SamochodElektryczny::~SamochodElektryczny()
{
	std::cout << "Destruktor samochodu elektrycznego" << std::endl;
}

int SamochodElektryczny::uruchom()
{
	if (silnik) {
		std::cout << "Nie moge uruchomic silnika elektrycznego bo jest juz uruchomiony" << std::endl;
		return -1;
	}
	else {
		silnik = 1;
		std::cout << "Uruchamiam silnik elektryczny" << std::endl;
		return 1;
	}
}

int SamochodElektryczny::tankuj()
{
	if (akumulator) {
		std::cout << "Nie moge naladowac akumulatora bo jest juz naladowany" << std::endl;
		return -1;
	}
	else {
		std::cout << "Laduje akumulator" << std::endl;
		akumulator = 1;
		return 1;
	}
}

int SamochodElektryczny::jedz()
{
	if (akumulator && silnik) {
		std::cout << "Jade na silniku elektrycznym" << std::endl;
		akumulator = 0;
		return 1;
	}
	
	if (!akumulator) {
		std::cout << "Nie moge jechac akumulator jest rozladowany" << std::endl;
	}
	
	if (!silnik) {
		std::cout << "Nie moge jechac bo silnik jest wylaczony" << std::endl;
	}

	return -1;
}

int SamochodElektryczny::stop()
{
	if (silnik) {
		std::cout << "Zatrzymuje silnik elektryczny" << std::endl;
		silnik = 0;
		return 1;
	}
	else {
		std::cout << "Nie moge zatrzymac wylaczonego silnika" << std::endl;
		return -1;
	}
}