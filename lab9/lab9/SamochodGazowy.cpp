#include "SamochodGazowy.h"

SamochodGazowy::SamochodGazowy() : butla(0)
{
	std::cout << "Konstruktor samochodu gazowego" << std::endl;
}

SamochodGazowy::SamochodGazowy(const SamochodGazowy&)
{
	std::cout << "Konstruktor kopiujacy samochodu gazowego" << std::endl;
}

SamochodGazowy::~SamochodGazowy()
{
	std::cout << "Destruktor samochodu gazowego" << std::endl;
}

int SamochodGazowy::uruchom()
{
	if (silnik) {
		std::cout << "Nie moge uruchomic silnika benzynowego bo jest juz uruchomiony" << std::endl;
		return -1;
	}
	else {
		silnik = 1;
		std::cout << "Uruchamiam silnik benzynowego" << std::endl;
		return 1;
	}
}

int SamochodGazowy::tankuj()
{
	if (butla) {
		std::cout << "Nie moge zatankowac butli bo jest juz pelny" << std::endl;
		return -1;
	}
	else {
		std::cout << "Tankuje gaz" << std::endl;
		butla = 1;
		return 1;
	}
}

int SamochodGazowy::jedz()
{
	if (butla && silnik) {
		std::cout << "Jade na silniku benzynowym" << std::endl;
		butla = 0;
		return 1;
	}

	if (!butla) {
		std::cout << "Nie moge jechac butla jest pusty" << std::endl;
	}

	if (!silnik) {
		std::cout << "Nie moge jechac bo silnik jest wylaczony" << std::endl;
	}

	return -1;
}

int SamochodGazowy::stop()
{
	if (silnik) {
		std::cout << "Zatrzymuje silnik benzynowy" << std::endl;
		silnik = 0;
		return 1;
	}
	else {
		std::cout << "Nie moge zatrzymac wylaczonego silnika benzynowego" << std::endl;
		return -1;
	}
}