#include "Samochod.h"

Samochod::Samochod() : silnik(0)
{
	std::cout << "Konstruktor samochod" << std::endl;
}

Samochod::Samochod(const Samochod& o)
{
	std::cout << "Konstruktor kopiujacy samochod" << std::endl;
}

Samochod::~Samochod()
{
	std::cout << "Destruktor samochod" << std::endl;
}

int Samochod::testSilnika()
{
	if (silnik == 1 || silnik == 0) {
		std::cout << "Silnik ok" << std::endl;
		return 1;
	}
	else {
		std::cout << "Blad silnika" << std::endl;
		return 0;
	}
}

void uszkodzenie(Samochod *s)
{
	s->silnik = -1;
}

void naprawa(Samochod *s)
{
	s->silnik = 0;
}