#include "SamochodElektrycznoBenzynowy.h"

SamochodElektrycznoBenzynowy::SamochodElektrycznoBenzynowy()
{
	std::cout << "Konstruktor samochodu hybdrydowego" << std::endl;
}

SamochodElektrycznoBenzynowy::SamochodElektrycznoBenzynowy(const SamochodElektrycznoBenzynowy&)
{
	std::cout << "Konstruktor kopiujacy samochodu hybdrydowego" << std::endl;
}

SamochodElektrycznoBenzynowy::~SamochodElektrycznoBenzynowy()
{
	std::cout << "Destruktor samochodu hybdrydowego" << std::endl;
}

int SamochodElektrycznoBenzynowy::uruchom()
{
	SamochodElektrycznoBenzynowy::uruchom();
	SamochodElektrycznoBenzynowy::uruchom();
	return 0;
}

int SamochodElektrycznoBenzynowy::tankuj()
{
	SamochodElektryczny::tankuj();
	SamochodBenzynowy::tankuj();
	return 0;
}

int SamochodElektrycznoBenzynowy::jedz()
{
	if (akumulator) {
		SamochodElektryczny::jedz();
		return 1;
	}
	else if (bak) {
		SamochodBenzynowy::jedz();
		return 1;
	}
	else {
		std::cout << "Nie moge jechac bo akumulator i bak sa puste" << std::endl;
		return 0;
	}

}

int SamochodElektrycznoBenzynowy::stop()
{
	SamochodElektryczny::stop();
	SamochodBenzynowy::stop();
	return 0;
}

int SamochodElektrycznoBenzynowy::testSilnika()
{
	int ret = 1;
	std::cout << "test benzyny";
	ret = SamochodBenzynowy::testSilnika() && ret;
	std::cout << "test elektryczny";
	ret = SamochodElektryczny::testSilnika() && ret;
	return ret;
}