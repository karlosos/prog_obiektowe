#pragma once
#include "SamochodBenzynowy.h"
#include "SamochodGazowy.h"
class SamochodBenzynowoGazowy :
	public SamochodBenzynowy, public SamochodGazowy
{
public:
	SamochodBenzynowoGazowy();
	SamochodBenzynowoGazowy(const SamochodBenzynowoGazowy&);
	virtual ~SamochodBenzynowoGazowy();
	int uruchom();
	int tankuj();
	int jedz();
	int stop();
	int testSilnika();
};

