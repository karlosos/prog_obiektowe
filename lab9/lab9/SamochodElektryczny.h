#pragma once
#include "Samochod.h"
class SamochodElektryczny : public Samochod
{
public:
	SamochodElektryczny();
	SamochodElektryczny(const SamochodElektryczny&);
	virtual ~SamochodElektryczny();
	int uruchom();
	int tankuj();
	int jedz();
	int stop();

protected:
	int akumulator;
};

