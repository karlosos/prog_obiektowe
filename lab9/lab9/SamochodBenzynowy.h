#pragma once
#include "Samochod.h"
class SamochodBenzynowy :
	public virtual Samochod
{
public:
	SamochodBenzynowy();
	SamochodBenzynowy(const SamochodBenzynowy&);
	virtual ~SamochodBenzynowy();
	int uruchom();
	int tankuj();
	int jedz();
	int stop();

protected:
	int bak;
};

