#include "SamochodBenzynowoElektryczny.h"

SamochodBenzynowoElektryczny::SamochodBenzynowoElektryczny()
{
	std::cout << "Konstruktor samochodu hybdrydowego" << std::endl;
}

SamochodBenzynowoElektryczny::SamochodBenzynowoElektryczny(const SamochodBenzynowoElektryczny&)
{
	std::cout << "Konstruktor kopiujacy samochodu hybdrydowego" << std::endl;
}

SamochodBenzynowoElektryczny::~SamochodBenzynowoElektryczny()
{
	std::cout << "Destruktor samochodu hybdrydowego" << std::endl;
}

int SamochodBenzynowoElektryczny::uruchom()
{
	SamochodBenzynowoElektryczny::uruchom();
	SamochodBenzynowoElektryczny::uruchom();
	return 0;
}

int SamochodBenzynowoElektryczny::tankuj()
{
	SamochodElektryczny::tankuj();
	SamochodBenzynowy::tankuj();
	return 0;
}

int SamochodBenzynowoElektryczny::jedz()
{
	if (akumulator) {
		SamochodElektryczny::jedz();
		return 1;
	}
	else if (bak) {
		SamochodBenzynowy::jedz();
		return 1;
	}
	else {
		std::cout << "Nie moge jechac bo akumulator i bak sa puste" << std::endl;
		return 0;
	}

}

int SamochodBenzynowoElektryczny::stop()
{
	SamochodElektryczny::stop();
	SamochodBenzynowy::stop();
	return 0;
}

int SamochodBenzynowoElektryczny::testSilnika()
{
	int ret = 1;
	std::cout << "test benzyny";
	ret = SamochodBenzynowy::testSilnika() && ret;
	std::cout << "test elektryczny";
	ret = SamochodElektryczny::testSilnika() && ret;
	return ret;
}