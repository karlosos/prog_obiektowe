#pragma once
#include <iostream>
class Samochod
{
public:
	Samochod();
	Samochod(const Samochod&);
	virtual ~Samochod();
	virtual int uruchom() = 0;
	virtual int tankuj() = 0;
	virtual int jedz() = 0;
	virtual int stop() = 0;
	int testSilnika();
	friend void uszkodzenie(Samochod *s);
	friend void naprawa(Samochod *s);
protected:
	int silnik;
};

