#include "SamochodBenzynowy.h"

SamochodBenzynowy::SamochodBenzynowy() : bak(0)
{
	std::cout << "Konstruktor samochodu benzynowego" << std::endl;
}

SamochodBenzynowy::SamochodBenzynowy(const SamochodBenzynowy&)
{
	std::cout << "Konstruktor kopiujacy samochodu benzynowego" << std::endl;
}

SamochodBenzynowy::~SamochodBenzynowy()
{
	std::cout << "Destruktor samochodu benzynowego" << std::endl;
}

int SamochodBenzynowy::uruchom()
{
	if (silnik) {
		std::cout << "Nie moge uruchomic silnika benzynowego bo jest juz uruchomiony" << std::endl;
		return -1;
	}
	else {
		silnik = 1;
		std::cout << "Uruchamiam silnik benzynowego" << std::endl;
		return 1;
	}
}

int SamochodBenzynowy::tankuj()
{
	if (bak) {
		std::cout << "Nie moge zatankowac baku bo jest juz pelny" << std::endl;
		return -1;
	}
	else {
		std::cout << "Tankuje benzyne" << std::endl;
		bak = 1;
		return 1;
	}
}

int SamochodBenzynowy::jedz()
{
	if (bak && silnik) {
		std::cout << "Jade na silniku benzynowym" << std::endl;
		bak = 0;
		return 1;
	}

	if (!bak) {
		std::cout << "Nie moge jechac bak jest pusty" << std::endl;
	}

	if (!silnik) {
		std::cout << "Nie moge jechac bo silnik jest wylaczony" << std::endl;
	}

	return -1;
}

int SamochodBenzynowy::stop()
{
	if (silnik) {
		std::cout << "Zatrzymuje silnik benzynowy" << std::endl;
		silnik = 0;
		return 1;
	}
	else {
		std::cout << "Nie moge zatrzymac wylaczonego silnika benzynowego" << std::endl;
		return -1;
	}
}