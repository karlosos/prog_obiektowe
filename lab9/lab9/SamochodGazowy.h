#pragma once
#include "Samochod.h"
class SamochodGazowy :
	public virtual Samochod
{
public:
	SamochodGazowy();
	SamochodGazowy(const SamochodGazowy&);
	virtual ~SamochodGazowy();
	int uruchom();
	int tankuj();
	int jedz();
	int stop();

protected:
	int butla;
};

