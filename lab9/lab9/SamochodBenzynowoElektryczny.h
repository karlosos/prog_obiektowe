#pragma once
#include "SamochodElektryczny.h"
#include "SamochodBenzynowy.h"
class SamochodBenzynowoElektryczny :
	public SamochodBenzynowy, public SamochodElektryczny
{
public:
	SamochodBenzynowoElektryczny();
	SamochodBenzynowoElektryczny(const SamochodBenzynowoElektryczny&);
	virtual ~SamochodBenzynowoElektryczny();
	int uruchom();
	int tankuj();
	int jedz();
	int stop();
	int testSilnika();
};

