#pragma once
class Osoba
{
public:
	Osoba();
	~Osoba();
	Osoba(const Osoba &o);
	void setImie(char *imie);
	char* getImie();
private:
	char *imie;
	void clear();
};

