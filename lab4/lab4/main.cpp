#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "Osoba.h"
#include "test.h"

int main() {
	Osoba osoba;
	osoba.setImie("Karol");
	std::cout << osoba.getImie() << std::endl;
	test(osoba);
	std::system("pause");
	return 0;
}