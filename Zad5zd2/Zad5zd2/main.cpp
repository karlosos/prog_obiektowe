#include "Int.h"
#include <iostream>

int main() {
	Int a(1), b(2), c;
	c = a;
	c = 2;
	int d;
	d = c;
	a = 3 + c;
	a = c + 3;
	std::cout << "Wartosc a: " << a << std::endl;
	return 0;
}