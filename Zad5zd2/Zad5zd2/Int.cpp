#include "Int.h"
// Konstruktor z wartoscia domyslna
Int::Int(int value)
{
	this->value = new int(value);
}
// Konstruktor kopiujacy
Int::Int(const Int & i)
{
	this->value = nullptr;
	if (i.value != nullptr)
		this->setValue(i.getValue());
}

Int::operator int() {
		return *(this->value);
}

// Destruktor
Int::~Int()
{
	clear();
}
// Czyszczenie pamieci zarezerwowanej przez obiekt na zewnatrz
void Int::clear()
{
	if (this->value != nullptr)
		delete(this->value);
}
// Setter pola value
void Int::setValue(int value)
{
	clear();
	this->value = new int(value);
}
// Getter pola value
int Int::getValue() const {
	return *(this->value);
}
// Przeciazenie operatora przypisania
Int& Int::operator=(const Int & i)
{
	this->setValue(i.getValue());
	return *this;
}
// Przeciazenie operatora przypisania z wartoscia calkowita
Int& Int::operator=(int value)
{
	this->setValue(value);
	return *this;
}

// Przeciazenie operatora dodawania
Int Int::operator+(const Int & i)
{
	return Int(getValue() + i.getValue());
}
Int Int::operator+(int value)
{
	return Int(getValue() + value);
}

// Przeciazenie operatora odejmowania
Int Int::operator-(const Int & i)
{
	return Int(getValue() - i.getValue());
}
Int Int::operator-(int value)
{
	return Int(getValue() - value);
}

// Przeciazenie operatora mnozenia
Int Int::operator*(const Int & i)
{
	return Int(getValue() * i.getValue());
}
Int Int::operator*(int value)
{
	return Int(getValue() * value);
}

// Przeciazenie operatora dzielenia
Int Int::operator/(const Int & i)
{
	return Int(getValue() / i.getValue());
}
Int Int::operator/(int value)
{
	return Int(getValue() / value);
}

// Przeciazenie operatora porownania
Int Int::operator==(const Int & i)
{
	return getValue() == i.getValue();
}
Int Int::operator==(int value)
{
	return getValue() == value;
}

// Przeciazenie operatora porownania
Int Int::operator!=(const Int & i)
{
	return getValue() != i.getValue();
}
Int Int::operator!=(int value)
{
	return getValue() != value;
}

// Przeciazenie operatora porownania
Int Int::operator>(const Int & i)
{
	return getValue() > i.getValue();
}
Int Int::operator>(int value)
{
	return getValue() > value;
}

// Przeciazenie operatora porownania
Int Int::operator>=(const Int & i)
{
	return getValue() >= i.getValue();
}
Int Int::operator>=(int value)
{
	return getValue() >= value;
}

// Przeciazenie operatora porownania
Int Int::operator<(const Int & i)
{
	return getValue() < i.getValue();
}
Int Int::operator<(int value)
{
	return getValue() < value;
}

// Przeciazenie operatora porownania
Int Int::operator<=(const Int & i)
{
	return getValue() <= i.getValue();
}
Int Int::operator<=(int value)
{
	return getValue() <= value;
}


std::ostream& operator<(std::ostream &str, const Int &ref)
{
	str << *(ref.value);
	return str;
}