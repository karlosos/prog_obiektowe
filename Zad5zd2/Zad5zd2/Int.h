#pragma once
#include <iostream>
class Int
{
public:
	// Konstruktor z domniemana wartoscia
	Int(int value = 0);
	// Konstruktor kopiujacy
	Int(const Int & i);
	// Destruktor
	~Int();
	// Setter pola value
	void setValue(int value);
	// Getter pola value
	int getValue() const;

	// Przeciazenie operatora przypisania. Przypisanie innym obiektem Int
	Int& operator=(const Int & i);
	// Przeciazenie operatora przypisania. Przypisanie wartoscia integer
	Int& operator=(int value);

	operator int();

	// Przeciazenie operatora dodawania
	Int operator+(const Int & i);
	Int operator+(int value);

	// Przeciazenie operatora odejmowania
	Int operator-(const Int & i);
	Int operator-(int value);

	// Przeciazenie operatora mnozenia
	Int operator*(const Int & i);
	Int operator*(int value);

	// Przeciazenie operatora dzielenia
	Int operator/(const Int & i);
	Int operator/(int value);

	// Przeciazenie operatora porownania
	Int operator==(const Int & i);
	Int operator==(int value);

	// Przeciazenie operatora roznosci
	Int operator!=(const Int & i);
	Int operator!=(int value);

	// Przeciazenie operatora mniejszosci
	Int operator<(const Int & i);
	Int operator<(int value);

	// Przeciazenie operatora mniejsze lub rowne
	Int operator<=(const Int & i);
	Int operator<=(int value);

	// Przeciazenie operatora wieksze
	Int operator>(const Int & i);
	Int operator>(int value);

	// Przeciazenie operatora wieksze lub rowne
	Int operator>=(const Int & i);
	Int operator>=(int value);

	friend std::ostream & operator<(std::ostream &, const Int &);
private:
	// Czyszczenie pamieci na zewnatrz zarezerwowanej przez obiekt
	void clear();
	int* value;
};

