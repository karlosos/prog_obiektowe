#include "Osoba.h"
#include <iostream>

/*
 Konstruktor - wywoływany przy tworzeniu obiektu
*/
Osoba::Osoba()
{
	std::cout << "Konstruktor" << std::endl;
}

/*
  Destruktor - wywoływany przy usuwaniu obiektu
*/
Osoba::~Osoba()
{
	std::cout << "Destruktor" << std::endl;
}
