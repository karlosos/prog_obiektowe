#include "Osoba.h"
#include <iostream>

int main() 
{
	{
		// Tworzenie obiektu osoba
		Osoba osoba;
		// Dynamiczne tworzenie obiektu osoba
		Osoba* osoba2 = new Osoba();
		// Po tworzeniu dynamicznym trzeba posprz�ta� (zwolni� pami�� i wywo�a� destruktor)
		delete(osoba2);
	}
	std::system("pause");
	return 0;
}