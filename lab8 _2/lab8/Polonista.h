#pragma once
#include "Ekspert.h"
#include <iostream>

class Polonista :
	public Ekspert
{
public:
	Polonista();
	Polonista(const Polonista&);
	~Polonista();
	char* dziedzina();
};

