#pragma once


// klasa abstrakcyjna posiada przynajmniej jedna metode abstrakcyjna
// metoda abstrakcyjna nie ma definicji
// = 0 dodac
// jedynie mozemy odziedziczyc
// tylko wtedy gdy klasa potomna przyslania metody abstrakcyjne (wszystkie)
// jezeli nie przysloni metody abstrakcyjnej to jest abstrakcyjna

#include <iostream>
class Ekspert
{
public:
	Ekspert();
	Ekspert(const Ekspert&);
	virtual ~Ekspert();
	virtual char* dziedzina() = 0;
};

