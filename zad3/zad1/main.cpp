#include "test.h"

int main()
{
	/*
	Wywolania funkcji nie sa jednoznaczne. Przeciazenie jest nieporawne.
	Severity	Code	Description	Project	File	Line	Suppression State
	Error (active)		cannot overload functions distinguished by return type alone	zad1	\\beta-d\users$\dk39259\Dokumenty\Visual Studio 2015\Projects\zad3\zad1\test.h	5
	*/
	char c = test();
	int i = test();
	return 0;
}