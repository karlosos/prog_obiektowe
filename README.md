# README #

Jak tego nie przeczytasz to nie skumasz o co biega.

### Co jest w folderach ###

folder | lab | opis
-------|---- | ---
[zad1](/zad1) | lab 1 | 
[zad2](/zad2) | lab 2 | Przeladowanie
[zad3](/zad3) | lab 2 | Przeladowanie funkcji ze wzgledu na typ (Nieprawidlowe)
[zad4](/zad4) | lab 2 | Przeladowanie funkcji z domniemaniem
[zad5](/zad5) | lab 2 | Niepoprawne przeladowania funkcji z domniemaniem
[zad6](/zad6) | lab 2 | Przeladowania funkcji z rzutowaniem
[zad7](/zad7) | lab 3 | Referencje
[zad8](/zad8) | lab 3 | Referencje
[zad9](/zad9) | lab 3 | Klasy
[lab4](/lab4) | lab 4 | Konstruktor kopiujący
[lab5](/lab5) | lab 4 | Konstruktor kopiujący (inicjalizacja obiektu)
[lab5zd](/Zad5) | lab 5 | zadanie domowe
[lab6](/Zad5zd2) | lab 6 | przeciazanie operatora >>
[lab6](/lab6) | lab 6 (kind of) | dziedziczenie
[lab6](/lab6_part2) | lab 6 (kind of) | dziedziczenie po dwoch ojcach